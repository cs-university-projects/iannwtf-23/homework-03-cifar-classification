# Model Configurations

## Image 1
![Image 1](./img/image1.png)

- **Optimizer:** Adam
- **Learning Rate:** 0.001
- **Architecture:** Two Conv2D Blocks

## Image 2
![Image 2](./img/image2.png)

- **Optimizer:** Adam
- **Learning Rate:** 0.01
- **Architecture:** Three Conv2D Blocks

## Image 3
![Image 3](./img/image3.png)

- **Optimizer:** Adam
- **Learning Rate:** 0.005
- **Architecture:** Two Conv2D Blocks

## Image 4
![Image 4](./img/image4.png)

- **Optimizer:** Adam
- **Learning Rate:** 0.001
- **Architecture:** Three Conv2D Blocks

## Image 5
![Image 5](./img/image5.png)

- **Optimizer:** SGD (Stochastic Gradient Descent)
- **Learning Rate:** 0.0005
- **Architecture:** Two Conv2D Blocks

## Image 6
![Image 6](./img/image6.png)

- **Optimizer:** SGD (Stochastic Gradient Descent)
- **Learning Rate:** 0.002
- **Architecture:** Three Conv2D Blocks

## Image 7
![Image 7](./img/image7.png)

- **Optimizer:** SGD (Stochastic Gradient Descent)
- **Learning Rate:** 0.001
- **Architecture:** Two Conv2D Blocks

## Image 8
![Image 8](./img/image8.png)

- **Optimizer:** SGD (Stochastic Gradient Descent)
- **Learning Rate:** 0.0001
- **Architecture:** Three Conv2D Blocks
