import pprint
import tqdm
import tensorflow as tf
import tensorflow_datasets as tfds

from src import config
from src.model import MyModel
from src.visualization import visualize


def prepare_cifar10_data(cifar10):
    # flatten the images into vectors
    # cifar10 = cifar10.map(lambda img, target: (tf.reshape(img, (-1,)), target))
    # convert data from uint8 to float32
    cifar10 = cifar10.map(lambda img, target: (tf.cast(img, tf.float32), target))
    # sloppy input normalization, just bringing image values from range [0, 255] to [-1, 1]
    cifar10 = cifar10.map(lambda img, target: ((img/128.)-1., target))
    # create one-hot targets
    cifar10 = cifar10.map(lambda img, target: (img, tf.one_hot(target, depth=10)))
    # cache this progress in memory, as there is no need to redo it; it is deterministic after all
    cifar10 = cifar10.cache()
    # shuffle, batch, prefetch
    cifar10 = cifar10.shuffle(1000)
    cifar10 = cifar10.batch(config.BATCH_SIZE)
    cifar10 = cifar10.prefetch(20)
    # return preprocessed dataset
    return cifar10


def training_loop(model, train_ds, val_ds, epochs):
    for epoch in range(epochs):
        print(f"Epoch {epoch + 1}:")

        # Training:
        for data in tqdm.tqdm(train_ds, position=0, leave=True):
            model.train_step(data)

        # print the metrics
        print([f"{m.name}: {m.result().numpy()}" for m in model.metrics])

        results = {
            f"train_{m.name}": m.result()
            for m in model.metrics
        }

        # reset all metrics (requires a reset_metrics method in the model)
        model.reset_metrics()

        # Validation:
        for data in val_ds:
            model.test_step(data)

        print([f"val_{m.name}: {m.result().numpy()}" for m in model.metrics])
        results |= {
            f"test_{m.name}": m.result()
            for m in model.metrics
        }

        # reset all metrics
        model.reset_metrics()
        print("\n")

        yield epoch, results


if __name__ == "__main__":
    # load dataset
    (train_ds, test_ds), ds_info = tfds.load('cifar10', split=['train', 'test'], as_supervised=True, with_info=True)

    # visualize dataset
    # tfds.visualization.show_examples(train_ds, ds_info)

    # create model
    model = MyModel()

    # start training
    training = training_loop(
        model,
        train_ds.apply(prepare_cifar10_data),
        test_ds.apply(prepare_cifar10_data),
        config.NUM_EPOCHS,
    )

    train_losses = []
    train_accuracies = []
    test_losses = []
    test_accuracies = []

    for item in training:
        epoch, results = item
        train_losses.append(results["train_loss"].numpy())
        train_accuracies.append(results["train_accuracy"].numpy())
        test_losses.append(results["test_loss"].numpy())
        test_accuracies.append(results["test_accuracy"].numpy())

    visualize(
        train_losses,
        train_accuracies,
        test_losses,
        test_accuracies,
    )
