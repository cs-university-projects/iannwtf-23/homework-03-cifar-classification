import tensorflow as tf
from keras.layers import Conv2D, MaxPooling2D, Dense, GlobalMaxPooling2D, Flatten

from .conv2d_block import Conv2DBlock
from . import config


class MyModel(tf.keras.Model):
    def __init__(self):
        super(MyModel, self).__init__()
        self.layers_ = [
            # Conv2DBlock(2, 16),
            # MaxPooling2D(),
            Conv2DBlock(2, 32),
            MaxPooling2D(),
            Conv2DBlock(2, 64),
            GlobalMaxPooling2D(),
            Dense(24, activation=tf.nn.relu),
            Dense(10, activation=tf.nn.softmax)
        ]

        self.optimizer = tf.keras.optimizers.SGD(learning_rate=config.LEARNING_RATE)
        # self.optimizer = tf.keras.optimizers.Adam(learning_rate=config.LEARNING_RATE)

        self.metrics_list = [
            tf.keras.metrics.Mean(name="loss"),
            tf.keras.metrics.CategoricalAccuracy(name="accuracy"),
        ]

        self.loss_function = tf.keras.losses.CategoricalCrossentropy(from_logits=True)


    @tf.function
    def __call__(self, x, training=False):
        for layer in self.layers_:
            x = layer(x)
        return x

    def reset_metrics(self):
        for metric in self.metrics:
            metric.reset_states()

    @tf.function
    def train_step(self, data):
        x, targets = data

        with tf.GradientTape() as tape:
            predictions = self(x, training=True)

            loss = self.loss_function(targets, predictions) + tf.reduce_sum(self.losses)

        gradients = tape.gradient(loss, self.trainable_variables)
        self.optimizer.apply_gradients(zip(gradients, self.trainable_variables))

        # update loss metric
        self.metrics[0].update_state(loss)

        # for all metrics except loss, update states (accuracy etc.)
        for metric in self.metrics[1:]:
            metric.update_state(targets, predictions)


    @tf.function
    def test_step(self, data):
        x, targets = data
        predictions = self(x, training=False)
        loss = self.loss_function(targets, predictions) + tf.reduce_sum(self.losses)

        self.metrics[0].update_state(loss)
        # for accuracy metrics:
        for metric in self.metrics[1:]:
            metric.update_state(targets, predictions)
